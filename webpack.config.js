const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

require('dotenv').config()

console.log(process.env.FLASK_ENV);

module.exports = {
  mode: process.env.FLASK_ENV,
  entry: './flaskpixi/src/js/app.js',
  devtool: 'source-map',
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'flaskpixi', 'static', 'js')
  },
  plugins: [
    new CopyPlugin([
      //{ from: 'node_modules/p5/lib/p5.min.js', to: 'lib' }
    ]),
  ]
};
