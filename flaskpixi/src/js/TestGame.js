import Game from './wdnch/Game';
import TestScene from './TestScene';

export default class TestGame extends Game {
    constructor() {
        super({
            tagId: 'game'
        })
    }

    setup() {
        this.testScene = new TestScene(this);
    }
}