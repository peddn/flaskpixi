import GameObject from './wdnch/GameObject';

export default class TestObject extends GameObject {

    constructor(game, scene, textureId) {
        super(game, scene, textureId, {
            x: 200,
            y: 30
        });
    }


}