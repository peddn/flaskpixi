import { TextStyle } from 'pixi.js';

import stylesData from './data/textstyles.json';

class TextStyles {
    constructor() {
        for(let styleName in stylesData) {
            this[styleName] = new TextStyle(stylesData[styleName]);
        }
    }

    get(styleName) {
        let style = this[styleName];
        if(style) {
            return style;
        } else {
            return new TextStyle({
                fontFamily: 'Arial',
                fontSize: 12
            });
        }
    }
}

export default new TextStyles();