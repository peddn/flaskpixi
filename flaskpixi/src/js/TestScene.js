import { Graphics } from 'pixi.js';

import Scene from './wdnch/Scene';
import TestObject from './TestObject';

export default class TestScene extends Scene {
    constructor(game) {
        super(game, {
            autorun: true,
            autoinject: true
        });
        
    }

    reset() {
        this.chest = new TestObject(this.game, this, 'chest');
        this.addChild(this.chest);
    }

    sceneLoop(delta) {
        super.sceneLoop(delta);
    }

}
