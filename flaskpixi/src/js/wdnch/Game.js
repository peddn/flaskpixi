import { Application } from 'pixi.js';

/**
 * Class representing a game. You should extend it.
 * @extends Game
 * @abstract
 */
export default class Game extends Application {
    /**
     * Creates a game.
     * @constructor
     * @param  {object} options - the options for the PIXI.Application
     */
    constructor(options = {}) {
        let config = {
            width: 1024, 
            height: 576,                       
            antialias: true, 
            transparent: false, 
            resolution: 1,
            backgroundColor: 0xFFFFFF
        }
        // override defaults with external options
        Object.assign(config, options);

        super(config);

        this.tagId = config.tagId;
        //Add the canvas that Pixi automatically created for you to the HTML document
        document.getElementById(this.tagId).appendChild(this.view);
    }

    
    /**
     * Loads the added resources.
     * @public
     * @async
     */
    async load() {
        await new Promise((resolve, reject) => {
            this.loader.load(resolve);
        });
    }

    /**
     * Override this Method, to inject your own setup code.
     * @override
     * @public
     */
    setup() {}
    
}
