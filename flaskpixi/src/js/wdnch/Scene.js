import { Container } from 'pixi.js';
import { Engine } from 'matter-js';

export default class Scene extends Container {

    // game: the game this scene is in
    // options: the options for this scene
    //     boolean autorun -> should the scene loop be started with object creation
    //     boolean autoinject -> should the 
    constructor(game, options = {}) {
        // the dafault configuration for a Scene
        let config = {
            autorun: true,
            autoinject: true
        }
        // override defaults with external options
        Object.assign(config, options);

        super();

        this.config = config;
        this.game = game;
        this.engine = Engine.create();
        this.world = this.engine.world;
        if(this.config.autorun) {
            this.run();
        }
        if(this.config.autoinject) {
            this.inject();
        }
        this.reset();
    }

    // @override
    sceneLoop(delta) {
        //console.log(delta);
        Engine.update(this.engine);
        this.children.forEach((child) => {
            child.update();
        });
    }

    // @override
    reset() {}

    inject() {
        this.game.stage.removeChild(this);
        this.game.stage.addChild(this);
    }

    eject() {
        this.game.stage.removeChild(this);
    }

    run() {
        this.game.ticker.remove(this.sceneLoop, this)
        this.game.ticker.add(this.sceneLoop, this);
    }

    pause() {
        this.game.ticker.remove(this.sceneLoop, this)
    }



}