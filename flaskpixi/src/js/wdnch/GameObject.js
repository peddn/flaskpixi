import { Sprite } from 'pixi.js';
import { World, Bodies } from 'matter-js';

export default class GameObject extends Sprite {
    constructor(game, scene, textureId, options = {}) {
        let config = {
            x: 100,
            y: 100
        }
        // override defaults with external options
        Object.assign(config, options);
        super(game.loader.resources[textureId].texture);
        this.config = config;
        this.game = game;
        this.scene = scene;
        this.body = Bodies.rectangle(this.config.x, this.config.y, this.width, this.height);
        World.add(this.scene.world, [this.body]);
        this.reset();
    }

    reset() {
        this.x = this.config.x;
        this.y = this.config.y;
    }

    update() {
        //console.log(this.body.position.x, this.body.position.y)
        this.x = this.body.position.x;
        this.y = this.body.position.y;
    }
}