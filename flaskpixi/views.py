import os

from flaskpixi import app

from flask import render_template, send_from_directory


@app.route('/materializecss/<path:filename>')
def materializecss_static(filename):
    return send_from_directory(os.path.join(app.root_path, '..', 'node_modules', 'materialize-css', 'dist', 'css'), filename)

@app.route('/')
def index():
    return render_template('index.html')